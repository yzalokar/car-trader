import { Component, OnInit } from '@angular/core';
import { CarService } from '../service/car.service';
import { Car } from '../data/car';
import { Observable } from 'rxjs'

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.displayCars();
  }

  async displayCars(){
    let carList: Car[] = [];
    await this.carService.getCars()
    .then((data) => {
      carList = data;
      console.log(carList);
    });
    console.log(carList.length);
  }


}
