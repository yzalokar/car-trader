export class Car{

    brand: String;
    model: String;
    version: String;
    hp: Number;
    cylinders: Number;
    engineSize: Number;
    weight: Number;
    bodyStyle: String;
    driveType: String;
    price: Number;

    carList: Car[];

    public constructor(brand: String, model: String, version: String, hp: Number, cylinders: Number, engineSize: Number, weight: Number, bodyStyle: String, driveType: String, price: Number){
        this.brand = brand;
        this.model = model;
        this.version = version;
        this.cylinders = cylinders;
        this.engineSize = engineSize;
        this.weight = weight;
        this.bodyStyle = bodyStyle;
        this.driveType = driveType;
        this.price = price;
    }

}
