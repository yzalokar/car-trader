import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { User } from '../data/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  loginBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private userService: UserService) { }

  ngOnInit() {

  }

  async login(){
    let userCheck: Number;
    this.loginBtnState = ClrLoadingState.LOADING;
    let user: User = new User(
      this.loginForm.get('username').value,
      this.loginForm.get('password').value
    );

    this.userService.login(user)
    .then((data) => {
      userCheck = data;
      console.log("Is logged in: " + data)
    });

    if(userCheck == 1){
      alert('Logged in successfully');
    }else {
      alert('Wrong username or password');
    }



  }

}
