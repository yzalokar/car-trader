import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { RegisterUser } from '../data/register-user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    fname: new FormControl('', [
      Validators.required,
    ]),
    lname: new FormControl('', [
      Validators.required,
    ]),
    username: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
    ]),
  });

  searchBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  async register(){
    this.loginBtnState = ClrLoadingState.LOADING;
    let user: RegisterUser = new RegisterUser(
      this.registerForm.get('fname').value,
      this.registerForm.get('lname').value,
      this.registerForm.get('username').value,
      this.registerForm.get('password').value
    );

    this.userService.register(user);
  }



}
