import { Injectable } from '@angular/core';
import { Car } from '../data/car'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const urlServer = 'http://127.0.0.1:8000';
const httpHeaders =  new HttpHeaders({
  'Content-Type':  'application/json',
});

@Injectable({
  providedIn: 'root'
})
export class CarService {

constructor(private http: HttpClient) { }

async uploadCar(car: Car) {
  const data = JSON.stringify({car});
  return await this.http.post(urlServer + '/api/create', data, { headers: httpHeaders })
  .toPromise();
}


async getCars(): Promise<Car[]> {
  const cars =  await this.http.get<Car[]>(urlServer + '/api/', { headers: httpHeaders , params: {}})
  .toPromise();
  return cars;
  }
}
