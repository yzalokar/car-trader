import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { User } from '../data/user';
import { RegisterUser } from '../data/register.user'

const urlServer = 'http://127.0.0.1:8000';
const httpHeaders =  new HttpHeaders({
  'Content-Type':  'application/json',
});

@Injectable({
  providedIn: 'root'
})
export class UserService {

constructor(private http: HttpClient) { }

async login(user: User): Promise<Number> {
  const data = JSON.stringify({user});
  return await this.http.post<Number>('/api/login', data, { headers: httpHeaders })
  .toPromise();
}

async register(user: RegisterUser) {
  const data = JSON.stringify({user});
  return await this.http.post('/api/register', data, { headers: httpHeaders })
  .toPromise();
}
}
