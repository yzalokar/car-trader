import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import { ClrLoadingState } from '@clr/angular';
import { CarService } from '../service/car.service';
import { Car } from '../data/car';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  uploadForm: FormGroup = new FormGroup({
    brand: new FormControl('', [
      Validators.required,
      Validators.minLength(2)
    ]),
    model: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    version: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    hp: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    cylinders: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    engineSize: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    weight: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ]),
    bodyStyle: new FormControl('4-door', [
      Validators.required,
    ]),
    driveType: new FormControl('front', [
      Validators.required,
    ]),
    price: new FormControl('', [
      Validators.required,
      Validators.minLength(1)
    ])
  });

  uploadBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  constructor(private carService: CarService) { }

  ngOnInit() {
  }

  onSubmit(){
    this.uploadBtnState = ClrLoadingState.LOADING;

    let car: Car = new Car(
      this.uploadForm.get('brand').value,
      this.uploadForm.get('model').value,
      this.uploadForm.get('version').value,
      this.uploadForm.get('hp').value,
      this.uploadForm.get('cylinders').value,
      this.uploadForm.get('engineSize').value,
      this.uploadForm.get('weight').value,
      this.uploadForm.get('bodyStyle').value,
      this.uploadForm.get('driveType').value,
      this.uploadForm.get('price').value
    );

    this.carService.uploadCar(car);
  }

}
